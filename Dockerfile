FROM scratch

ADD bottin.static /bottin

ENTRYPOINT ["/bottin"]
