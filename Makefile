BIN=bottin
SRC=main.go ssha.go util.go acl.go read.go write.go memberof.go
DOCKER=lxpz/bottin_amd64

all: $(BIN)

$(BIN): $(SRC)
	go get -d -v
	go build -v -o $(BIN)

$(BIN).static: $(SRC)
	go get -d -v
	CGO_ENABLED=0 GOOS=linux go build -a -v -o $(BIN).static

docker: $(BIN).static
	docker build -t $(DOCKER):$(TAG) .
	docker push $(DOCKER):$(TAG)
	docker tag $(DOCKER):$(TAG) $(DOCKER):latest
	docker push $(DOCKER):latest
