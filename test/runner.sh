#!/bin/sh

set -ex

echo $BOTTIN_DEFAULT_ADMIN_PW
consul agent -dev > /dev/null 2>&1 &
sleep 2
cp test/config.json.test config.json
./bottin > /dev/null 2>&1 &
sleep 1
./test/test -test.v -test.failfast -test.short -test.run TestPrincipal
./test/test -test.v -test.failfast -test.run TestPrincipal/B=
