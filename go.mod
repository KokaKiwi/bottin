module bottin

go 1.13

require (
	github.com/go-ldap/ldap/v3 v3.3.0
	github.com/google/uuid v1.1.1
	github.com/hashicorp/consul/api v1.3.0
	github.com/lor00x/goldap v0.0.0-20180618054307-a546dffdd1a3
	github.com/sirupsen/logrus v1.4.2
)
